package com.atlassian.jira.plugins.useradmin.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 */
@XmlRootElement
public class FilteredUsersBean {

    @XmlElement
    private int resultsCount;

    @XmlElement
    private Iterable<UserExtendedBean> users;

    public FilteredUsersBean(int resultsCount, Iterable<UserExtendedBean> users) {
        this.resultsCount = resultsCount;
        this.users = users;
    }
}

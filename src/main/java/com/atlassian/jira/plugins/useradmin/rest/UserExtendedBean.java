package com.atlassian.jira.plugins.useradmin.rest;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugins.useradmin.rest.bind.DateTimeAdapter;
import com.atlassian.jira.rest.v2.issue.GroupBean;
import com.atlassian.jira.rest.v2.issue.UserBean;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.net.URI;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

public class UserExtendedBean extends UserBean {

    private User user;

    private static class GroupNameToBean implements Function<String, GroupBean> {

        @Override
        public GroupBean apply(@Nullable String input) {
            return new GroupBean(input);
        }
    }
    @XmlElement
    private long loginCount;

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    @XmlElement
    private DateTime lastLoggedInTime;

    @XmlElement
    private Collection<String> groupNames;

    @XmlElement
    private Map<String, Set<String>> projectRoles;

    @XmlElement
    private String emailAddress;

    @XmlElement
    private String displayName;

    public UserExtendedBean(URI self, String name, String displayName, boolean active, Map<String, URI> avatarUrls, long loginCount, DateTime lastLoggedInTime) {
        super(self, name, displayName, active, avatarUrls);
        this.loginCount = loginCount;
        this.lastLoggedInTime = lastLoggedInTime;
    }

    public
    UserExtendedBean(URI self, String name, String displayName, boolean active, String emailAddress, Collection<String> groupNames,
                            Map<String, URI> avatarUrls, TimeZone timeZone, long loginCount, DateTime lastLoggedInTime,
                            Map<String, Set<String>> projectRoles, User user) {
        super(self, name, displayName, active, emailAddress, Lists.newArrayList(Iterables.transform(groupNames, new GroupNameToBean())), avatarUrls, timeZone);
        this.loginCount = loginCount;
        this.lastLoggedInTime = lastLoggedInTime;
        this.groupNames = groupNames;
        this.projectRoles = projectRoles;
        this.user = user;
        this.emailAddress = emailAddress;
        this.displayName = displayName;
    }

    public Collection<String> getGroupNames() {
        return groupNames;
    }

    public Map<String, Set<String>> getProjectRoles() {
        return projectRoles;
    }

    public User getUser() {
        return user;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getDisplayName() {
        return displayName;
    }
}

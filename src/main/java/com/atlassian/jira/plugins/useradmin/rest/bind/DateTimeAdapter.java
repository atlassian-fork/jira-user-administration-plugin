package com.atlassian.jira.plugins.useradmin.rest.bind;

import com.atlassian.jira.rest.Dates;
import org.joda.time.DateTime;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DateTimeAdapter extends XmlAdapter<String, DateTime>
{
    @Override
    public DateTime unmarshal(final String s) throws Exception
    {
        return new DateTime(Dates.fromTimeString(s));
    }

    @Override
    public String marshal(final DateTime date) throws Exception
    {
        return Dates.asTimeString(date.toDate());
    }
}

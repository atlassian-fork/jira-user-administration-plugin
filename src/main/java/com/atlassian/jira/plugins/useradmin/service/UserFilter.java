package com.atlassian.jira.plugins.useradmin.service;

import com.atlassian.jira.bc.user.search.UserMatcherPredicate;
import com.atlassian.jira.plugins.useradmin.rest.UserExtendedBean;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;

/**
 *
 */
public class UserFilter implements Predicate<UserExtendedBean> {

    private Predicate<UserExtendedBean> predicate;

    public UserFilter(Predicate<UserExtendedBean> predicate) {
        this.predicate = predicate;
    }

    @Override
    public boolean apply(@Nullable UserExtendedBean userBean) {
        return predicate.apply(userBean);
    }

    public static UserFilter createFilter(List<String> groups, List<String> roles, List<String> projects, List<String> userNames) {
        Predicate<UserExtendedBean> andPredicate = Predicates.and(rolesFilters(projects, roles), groupFilters(groups),
                projectFilters(projects), usernameFiler(userNames));
        return new UserFilter(andPredicate);
    }

    private static Predicate<UserExtendedBean> rolesFilters(List<String> projectKeys, final Iterable<String> roles) {
        if (roles == null && projectKeys == null) {
            return Predicates.alwaysTrue();
        } else if (projectKeys == null) {
            final Set<String> rolesSet = Sets.newHashSet(roles);
            return new Predicate<UserExtendedBean>() {

                @Override
                public boolean apply(final UserExtendedBean userBean) {
                    for (Set<String> userProjectRoles : userBean.getProjectRoles().values()) {
                        if (!Sets.intersection(userProjectRoles, rolesSet).isEmpty()) {
                            return true;
                        }
                    }
                    return false;
                }
            };
        } else if (roles == null) {
            return Predicates.alwaysTrue();
        }
        final Set<String> rolesSet = Sets.newHashSet(roles);
        return Predicates.or(Iterables.transform(projectKeys, new Function<String, Predicate<UserExtendedBean>>() {
            @Override
            public Predicate<UserExtendedBean> apply(final String projectKey) {
                return new Predicate<UserExtendedBean>() {
                    @Override
                    public boolean apply(final UserExtendedBean user) {
                        final Set<String> projectRoles = user.getProjectRoles().get(projectKey);
                        return projectRoles != null && !Sets.intersection(projectRoles, rolesSet).isEmpty();
                    }
                };
            }
        }));
    }

    private static Predicate<UserExtendedBean> groupFilters(List<String> groups) {
        if (groups == null) {
            return Predicates.alwaysTrue();
        }
        return Predicates.or(Iterables.transform(groups, new Function<String, Predicate<UserExtendedBean>>() {
            @Override
            public Predicate<UserExtendedBean> apply(final String group) {
                return new Predicate<UserExtendedBean>() {
                    @Override
                    public boolean apply(final UserExtendedBean user) {
                        return user.getGroupNames().contains(group);
                    }
                };
            }
        }));
    }

    private static Predicate<UserExtendedBean> projectFilters(List<String> projectKeys) {
        if (projectKeys == null) {
            return Predicates.alwaysTrue();
        }
        return Predicates.or(Iterables.transform(projectKeys, new Function<String, Predicate<UserExtendedBean>>() {
            @Override
            public Predicate<UserExtendedBean> apply(final String projectKey) {
                return new Predicate<UserExtendedBean>() {
                    @Override
                    public boolean apply(final UserExtendedBean user) {
                        return user.getProjectRoles().containsKey(projectKey);
                    }
                };
            }
        }));
    }

    private static Predicate<UserExtendedBean> usernameFiler(List<String> userNames) {
        if (userNames == null) {
            return Predicates.alwaysTrue();
        }
        return Predicates.or(Iterables.transform(userNames, new Function<String, Predicate<UserExtendedBean>>() {
            @Override
            public Predicate<UserExtendedBean> apply(final String userName) {
                return new Predicate<UserExtendedBean>() {
                    @Override
                    public boolean apply(final UserExtendedBean userBean) {
                        return new UserMatcherPredicate(userName, true).apply(userBean.getUser());
                    }
                };
            }
        }));
    }

}

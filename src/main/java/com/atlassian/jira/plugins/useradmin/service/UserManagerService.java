package com.atlassian.jira.plugins.useradmin.service;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.impl.ImmutableUser;
import com.atlassian.crowd.exception.*;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.security.login.LoginInfo;
import com.atlassian.jira.bc.security.login.LoginService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.exception.*;
import com.atlassian.jira.exception.PermissionException;
import com.atlassian.jira.plugins.useradmin.rest.FilteredUsersBean;
import com.atlassian.jira.plugins.useradmin.rest.UserExtendedBean;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.collect.MapBuilder;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.joda.time.DateTime;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

/**
 *
 */
public class UserManagerService {

    private final UserManager userManager;
    private final LoginService loginService;
    private final UserUtil userUtil;
    private final TimeZoneManager timeZoneManager;
    private final ProjectRoleManager projectRoleManager;
    private final ProjectManager projectManager;
    private final GroupManager groupManager;
    private final CrowdService crowdService;

    public UserManagerService(final UserManager userManager, final LoginService loginService, final UserUtil userUtil,
                              final TimeZoneManager timeZoneManager, final ProjectRoleManager projectRoleService,
                              final ProjectManager projectManager, final GroupManager groupManager,
                              final CrowdService crowdService) {
        this.userManager = userManager;
        this.loginService = loginService;
        this.userUtil = userUtil;
        this.timeZoneManager = timeZoneManager;
        this.projectRoleManager = projectRoleService;
        this.projectManager = projectManager;
        this.groupManager = groupManager;
        this.crowdService = crowdService;
    }

    public Iterable<UserExtendedBean> getAllUsers() {
        return Iterables.transform(userManager.getAllUsers(), new UserToUserBeanFunction());
    }

    public UserExtendedBean getUserByName(final String username) {
        return new UserToUserBeanFunction().apply(userManager.getUser(username));
    }

    public FilteredUsersBean getUsers(final UserFilter filter, final int startAt, final int limit) {
        final Iterable<UserExtendedBean> filteredUsers = Iterables.filter(getAllUsers(), filter);
        return new FilteredUsersBean(Iterables.size(filteredUsers), Iterables.limit(Iterables.skip(filteredUsers, startAt), limit));
    }

    public void addUsersToGroups(List<String> userNames, List<String> groupNames) throws OperationNotPermittedException, UserNotFoundException, GroupNotFoundException, OperationFailedException {
        for (final String userName : userNames) {
            final User user = userUtil.getUser(userName);
            for (final String groupName : groupNames) {
                final Group group = groupManager.getGroup(groupName);
                groupManager.addUserToGroup(user, group);
            }
        }
    }

    public void removeUsersFromGroups(List<String> userNames, List<String> groupNames) throws OperationNotPermittedException, UserNotFoundException, GroupNotFoundException, OperationFailedException, PermissionException, RemoveException {
        for (final String userName : userNames) {
            final User user = userUtil.getUser(userName);
            for (final String groupName : groupNames) {
                final Group group = groupManager.getGroup(groupName);
                userUtil.removeUserFromGroup(group, user);
            }
        }
    }

    public void addUser(String username, String password, String fullName, String email, boolean sendNotification) throws OperationNotPermittedException, InvalidUserException, InvalidCredentialException {
        final User user = new ImmutableUser.Builder()
                .name(username)
                .emailAddress(email)
                .active(true)
                .displayName(fullName)
                .toUser();
        crowdService.addUser(user, password);
    }

    private class UserToUserBeanFunction implements Function<User, UserExtendedBean> {

        @Override
        public UserExtendedBean apply(final User user) {
            final LoginInfo loginInfo = loginService.getLoginInfo(user.getName());
            SortedSet<String> groups = (userUtil.getGroupNamesForUser(user.getName()));
            Map<String, Set<String>> projectRoles = getProjectRoles(user);

            return new UserExtendedBean(null, user.getName(), user.getDisplayName(), user.isActive(), user.getEmailAddress(),
                    groups, getAvatarUris(user), timeZoneManager.getLoggedInUserTimeZone(),
                    loginInfo.getLoginCount() != null ? loginInfo.getLoginCount() : 0, new DateTime(loginInfo.getLastLoginTime()),
                    projectRoles, user);
        }
    }

    private Map<String, Set<String>> getProjectRoles(final User user) {
        final ImmutableMap.Builder<String, Set<String>> builder = ImmutableMap.builder();

        for (Project project : projectManager.getProjectObjects()) {
            final Set<String> projectRoleNames = Sets.newHashSet(Iterables.transform(projectRoleManager.getProjectRoles(user, project),
                    new Function<ProjectRole, String>() {
                        @Override
                        public String apply(final ProjectRole projectRole) {
                            return projectRole.getName();
                        }
                    })
            );
            if (projectRoleNames != null && projectRoleNames.size() > 0) {
                builder.put(project.getKey(), projectRoleNames);
            }
        }

        return builder.build();
    }

    private Map<String, URI> getAvatarUris(User user) {
        final AvatarService avatarService = ComponentAccessor.getAvatarService();

        return MapBuilder.<String, URI>newBuilder()
                .add("16x16", avatarService.getAvatarAbsoluteURL(user, user.getName(), Avatar.Size.SMALL))
                .add("48x48", avatarService.getAvatarAbsoluteURL(user, user.getName(), Avatar.Size.LARGE))
                .toMap();

    }

}

(function($){
    $(function(){

        var userTableModel = new AJS.Plugins.useradmin.models.userTable();
        //pre-populate all groups field
        //todo: do it better
        var groups = [];
        $("#groups-dropdown option").each(function(i,el){
            groups.push($(el).val())
        });
        userTableModel.selectedUsers.set("allGroups", groups);



        var userTable = new AJS.Plugins.useradmin.views.table.userTable({model:userTableModel});

        $(".user-admin-container").prepend(userTable.$el);
        userTableModel.fetch();

        var filter = new AJS.Plugins.useradmin.views.table.filter({model : userTableModel.filterModel});

        var sidePanel = new AJS.Plugins.useradmin.views.sidepanel.panel({tableModel : userTableModel});


        var dialog = new JIRA.FormDialog({
            trigger: "#create_user",
            onDialogFinished : function(){
                userTableModel.fetch();
                this.hide();
            }
        });

        //preventing redirections in the dirtiest possible way
        dialog._detectRedirectInstructions = function(xhr) {
            var instructions = {
                serverIsDone : false,
                redirectUrl : false

            };
            var doneHeader = xhr.getResponseHeader('X-Atlassian-Dialog-Control');
            if (doneHeader) {
                instructions.serverIsDone = true;
            }
            return instructions;
        }

    });

})((AJS.$));

(function($){
    AJS.namespace("AJS.Plugins.useradmin.views.table");

    var UserTableRow = Backbone.View.extend({

        initialize : function(){
            var date = new Date(this.model.get("lastLoggedInTime"));
            this.model.set("formattedLastLoggedInTime", date.getFullYear()+"-"+date.getMonth()+1+"-"+date.getDate());
            this.model.bind("change:selected", this.selectionChanged, this);
        },

        selectionChanged : function(){
            this.$el.find(".useradmin-selectuser input").prop("checked", this.model.get("selected"));
        },


        tagName : "tr",

        events : {
            "click .userlink" : "userClicked",
            "click .useradmin-selectuser" : "userCheckboxClicked"
        },

        userClicked : function(){
            this.options.parent.onUserClicked(this);
        },

        userCheckboxClicked : function(){
            this.options.parent.onUserCheckboxClicked(this);
        },

        className : "vcard user-row",

        render : function() {
            var m = this.model;
            var content = AJS.Plugins.useradmin.templates.userTableRow({
                user:this.model.attributes,
                token: atl_token()
            });

            this.$el.html(content);
            this.selectionChanged();
            return this;
        }

    });


    var UserTableView = Backbone.View.extend({
        className: "aui-item",

        events : {
            "change #selectAll" : "selectAllChanged"
        },

        initialize : function() {
            this.model.bind("fetch", this.render, this);
            this.model.bind("change:selection", this.onSelectionChanged, this);
        },

        renderTable : function(){
            var content = AJS.Plugins.useradmin.templates.userTable({
                users:this.model.attributes,
                token : atl_token()
            });
            this.$el.html(content);

            var selectAllDialogContent = $(AJS.Plugins.useradmin.templates.selectAllDialog());
            this.selectAllInlineDialog = new AJS.InlineLayer({
                offsetTarget: this.$el.find("#selectAll"),
                content: selectAllDialogContent
            });


            //bind helper elements
            this.$tbody = this.$el.find("tbody");
            this.$counters = this.$el.find(".results-count");
            this.$pagination = this.$el.find(".pagination");
            this.$selectAllCheckbox = selectAllDialogContent.find("#selectAllResults");
            this.$selectCheckbox = this.$el.find("#selectAll");

            var that = this;
            this.$selectAllCheckbox.bind("change", function(){
                that.selectAllResultsChanged();
            });


        },


        renderCounters : function(){
            var startAt = this.model.filterModel.get("startAt");
            var perPage = this.model.filterModel.get("limit");
            var all = this.model.get("resultsCount");

            var first = startAt+1;
            var last = Math.min(all, startAt+perPage);

            this.$counters.find(".results-count-start").text(first);
            this.$counters.find(".results-count-end").text(last);
            this.$counters.find(".results-count-total").text(all);

            this.$pagination.empty();

            var that = this;

            var pages = Math.ceil(all/perPage);
            for(var i =0; i<pages; i++){
                var pageLink = $("<a href='#'></a>");
                pageLink.text(i+1);
                var thisLinkStartAt = i*perPage;
                pageLink.data("start", thisLinkStartAt);
                if(startAt == thisLinkStartAt){
                    pageLink.addClass("current-page");
                }
                pageLink.click(function(e){
                    var newStartAt = $(this).data("start");
                    that.model.filterModel.set("startAt", newStartAt);
                    e.preventDefault();

                });

                this.$pagination.append(pageLink);
            }

        },

        render : function(){
            this.renderTable();
            this.renderCounters();

            var that = this;
            this.model.users.each(function(user){
                that.addSingleRow(user);
            });
            return this;
        },


        addSingleRow : function(userModel){
            var userTableRow = new UserTableRow({
                model: userModel,
                parent : this
            });
            userTableRow.render();
            var name = userTableRow.$el;
            this.$tbody.append(name);
        },

        selectAllChanged : function(){
            var checked = this.$selectCheckbox.prop("checked");
            this.model.selectAllToggle(checked);
            if(checked){
                this.selectAllInlineDialog.show();
            } else {
                this.selectAllInlineDialog.hide();
            }
        },

        selectAllResultsChanged : function(){
            this.model.selectAllResultsToggle(this.$selectAllCheckbox.prop("checked"));
        },

        onSelectionChanged : function(){
            var selectedUsersCount = this.model.selectedUsers.users.length;
            if(selectedUsersCount == 0){
                this.$selectCheckbox.prop("checked", false);
                this.$selectAllCheckbox.prop("checked", false);
            }
        },

        onUserClicked : function(userView){
            this.model.selectUser(userView.model);
        },

        onUserCheckboxClicked : function(userView){
            this.model.addUserToSelection(userView.model);
        }

    });


    var FilterView =  Backbone.View.extend({

        events : {
            "submit .text-query-container form" : "userNamesChanged",
            "unselect #projects-dropdown" : "triggerProjects",
            "selected #projects-dropdown" : "triggerProjects",
            "unselect #groups-dropdown" : "triggerGroups",
            "selected #groups-dropdown" : "triggerGroups",
            "unselect #roles-dropdown" : "triggerRoles",
            "selected #roles-dropdown" : "triggerRoles"

        },

        triggerProjects : function(){
            var val = $("#projects-dropdown").val();
            this.model.set("projects", val);
        },
        triggerGroups : function(){
            var val = $("#groups-dropdown").val();
            this.model.set("groups", val);
        },
        triggerRoles : function(){
            var val = $("#roles-dropdown").val();
            this.model.set("roles", val);
        },

        userNamesChanged : function(e){
            e.preventDefault();
            var value = this.$el.find("#searcher-query").val();
            this.model.set("userNames", value);
        },

        initialize : function(){
            this.setElement($(".user-admin-toolbar")[0]);

            new AJS.CheckboxMultiSelect({
                element: this.$el.find("#projects-dropdown")
            }).$container.addClass("ajs-layer box-shadow").hide();

            new AJS.CheckboxMultiSelect({
                element: this.$el.find("#groups-dropdown")
            }).$container.addClass("ajs-layer box-shadow").hide();

            new AJS.CheckboxMultiSelect({
                element: this.$el.find("#roles-dropdown")
            }).$container.addClass("ajs-layer box-shadow").hide();

        }
    });



    AJS.Plugins.useradmin.views.table.userTable = UserTableView;
    AJS.Plugins.useradmin.views.table.userTableRow = UserTableRow;
    AJS.Plugins.useradmin.views.table.filter = FilterView;

})(AJS.$);